import gitlab

# Connect read-only
gl = gitlab.Gitlab('https://gitlab.com')
if gl is None:
    print('Gitlab was not found')
    exit(1)

# Tezos good values:
# project_id = 3836952
# project_branch = 'master'

# Test project: https://gitlab.com/vch9/coverage-on-master
project_id = 30973998
project_branch = 'main'

# Get project
project = gl.projects.get(project_id)
if project is None:
    print('Project was not found')
    exit(1)

# Last commit on master is the merge commit, we take last + 1
# this should be the last commit used in the wanted pipeline
commits = project.commits.list(ref_name=project_branch)
if commits is None or len(commits) < 2:
    print('Commits are not found or less than 2 commits')
    exit(1)

commit = commits[1]
commit_id = commit.id

# Get firt/last? merge request associated with the commit
mrs = commit.merge_requests()
if mrs is None or len(mrs) < 1:
    print('No merge request found for ' + str(commit_id))
    exit(1)

mr = mrs[0]
mr_id = mr['id']

# Last merge request merged
pipeline = None
for x in project.pipelines.list():
    if x.sha == commit_id:
        pipeline = x
        break

if pipeline is None:
    print('Pipeline not found for ' + str(commit_id))
    exit(1)

# List of jobs
jobs = pipeline.jobs.list()
if jobs is None or len(jobs) < 0:
    print('Jobs not found')
    exit(1)

coverage_job_name = 'make_coverage'
coverage = None

for job in jobs:
    if job.name == coverage_job_name:
        coverage = job.coverage

print('Coverage ' + str(coverage) + "%")
